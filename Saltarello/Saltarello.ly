% Copyright © 2013, Loïc Grobol <loic.grobol@gmail.com>
%
% Permission is granted to Do What The Fuck You Want To
% with this document.
%
% See the WTF Public License, Version 2 as published by Sam Hocevar
% at http://wtfpl.net if you need more details.

\version "2.12.0"

#(set-default-paper-size "a4")

\paper {
  line-width    = 190\mm
  left-margin   = 10\mm
  top-margin    = 10\mm
  bottom-margin = 20\mm
  %%indent = 0 \mm 
  %%set to ##t if your score is less than one page: 
  ragged-last-bottom = ##t 
  ragged-bottom = ##f  
  %% in orchestral scores you probably want the two bold slashes 
  %% separating the systems: so uncomment the following line: 
  %% system-separator-markup = \slashSeparator 
  }

\header {
    title = "Saltarello"
    composer = "Trad (inspired by Řemdih's version)"
    copyright = \markup \center-column {
                          "Copyright © Loïc Grobol 2013," 
                          "You can do What The Fuck You Want with this document, which is licensed under the"
                          "What The Fuck Public Licence, version 2 as published by Sam Hocevar"
                          "If you want more details on this licence, see its text at http://www.wtfpl.net/txt/copying"
      }
    }

AFlvoiceAA = \relative c'{
    \set Staff.instrumentName = #"Flute"
    \set Staff.shortInstrumentName = #"Fl."
    \clef treble
    %staffkeysig
    \key d \major 
    %barkeysig: 
    \key d \major 
    %bartimesig: 
    \time 4/4 
    \repeat volta 2 { 
      g'8 a b e, b' e, b' e,	| % 1
      \mark \markup { \musicglyph #"scripts.segno" } 
      b'4 a8 fis16 g a g fis g e8 b'	|% 2
      e,4 e8 fis g16 fis g e d8 a'	| % 3
      d,4 e a8. fis16 g4	| % 4
    }\alternative{
        {
          fis16 e d e fis4 b fis	| % 5
        }{     
          fis16 e fis g fis4 b fis	| % 6
      }
    }
    fis8 g a e a e a e	| % 7
    a16 g fis g e8 b' e,4 a16 g fis g	| % 8
    fis e fis g a8 e a e a e	| % 9
    a16 g fis g e8 b' e,4 d16 e fis g	| % 10
    e16 fis g a b8 e, b' e, b' e,	| % 11
    \repeat volta 2 {
      b'4 a8 fis16 g a g fis g e8 b'	| % 12
      e,4 e8 fis g16 fis g e d8 a'	| % 13
      d,4 e a8. fis16 g4	| % 14
      fis16 e d e fis4 b fis	| % 15
    }\alternative{
      {
        g8 a b e, b' e, b' e, 	| %16
      }{
        cis'8 d e2.~      | % 17
      }
    }
    \repeat volta 2 {
        e1~	| %18
        e8 d cis16 d e d cis d e d cis d e cis	| %19
    }\alternative{
        {
          d16 e d cis d cis b cis d8 b e4~	| %20
        }{
          d16 e d cis b a g a b8 e, b' e,	| % 21
        }
    }
    \once\override Score.RehearsalMark #'self-alignment-X = #RIGHT 
    \mark \markup{Dal segno \raise #2 \halign#-1 \musicglyph #"scripts.segno"} 
    \repeat volta 2 {
        b'4 a8 fis16 g a g fis g e8 b'      | % 22
        e,4 e8 fis g16 fis g e d8 a' 	| %23
        d,4 e a8. fis16 g4 	| % 24
    } \alternative{
        {
          fis16 e d e fis4 b fis 	| % 25
          g8 a b e, b' e, b' e,	|  %26
        }{
          fis16 e fis g fis4 b e^\fermata  \bar "|." % 27
        }
      }
}% end of last bar in partorvoice

\score { 
    << 
        \context Staff = AFlpartA << 
            \context Voice = AFlvoiceAA \AFlvoiceAA
        >>

      \set Score.skipBars = ##t
      %%\set Score.melismaBusyProperties = #'()
      \override Score.BarNumber #'break-visibility = #end-of-line-invisible %%every bar is numbered.!!!
      %% remove previous line to get barnumbers only at beginning of system.
       #(set-accidental-style 'modern-cautionary)
      \set Score.markFormatter = #format-mark-box-letters %%boxed rehearsal-marks
       \override Score.TimeSignature #'style = #'() %%makes timesigs always numerical
      %% remove previous line to get cut-time/alla breve or common time 
      \set Score.pedalSustainStyle = #'mixed 
       %% make spanners comprise the note it end on, so that there is no doubt that this note is included.
       \override Score.TrillSpanner #'(bound-details right padding) = #-2
      \override Score.TextSpanner #'(bound-details right padding) = #-1
      %% Lilypond's normal textspanners are too weak:  
      \override Score.TextSpanner #'dash-period = #1
      \override Score.TextSpanner #'dash-fraction = #0.5
      %% lilypond chordname font, like mscore jazzfont, is both far too big and extremely ugly (olagunde@start.no):
      \override Score.ChordName #'font-family = #'roman 
      \override Score.ChordName #'font-size =#0 
      %% In my experience the normal thing in printed scores is maj7 and not the triangle. (olagunde):
      \set Score.majorSevenSymbol = \markup {maj7}
  >>

  %% Boosey and Hawkes, and Peters, have barlines spanning all staff-groups in a score,
  %% Eulenburg and Philharmonia, like Lilypond, have no barlines between staffgroups.
  %% If you want the Eulenburg/Lilypond style, comment out the following line:
  \layout {\context {\Score \consists Span_bar_engraver}}
}%% end of score-block 

#(set-global-staff-size 20)
